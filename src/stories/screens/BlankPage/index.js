import * as React from "react";
import { View, Form, Textarea, Container, Header, Title, Content, Text, Button, Icon, Left, Right, Body } from "native-base";
import styles from "./styles";

export interface Props {
	navigation: any;
}
export interface State {}
class BlankPage extends React.Component<Props, State> {
	render() {
		const param = this.props.navigation.state.params;
		const comp = this.props.comp;
		const pid = this.props.pid;
		const title = this.props.title;

		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="ios-arrow-back" />
						</Button>
					</Left>
					<Body style={{ flex: 3 }}>
						<Title>{title ? title : "Blank Page"}</Title>
					</Body>
					<Right />
				</Header>
				<Content padder>
						{comp}
				</Content>
			</Container>
		);
	}
}

export default BlankPage;

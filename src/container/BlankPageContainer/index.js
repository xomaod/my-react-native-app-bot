// @flow
import * as React from "react";
import { Button, Form, Textarea, Toast, Text, Item, Input, View } from "native-base"
import { Field, reduxForm } from "redux-form";
import BlankPage from "../../stories/screens/BlankPage";

const required = value => (value ? undefined : "Required");

export interface Props {
	navigation: any,
}
export interface State {}

export default class BlankPageContainer extends React.Component<Props, State> {

	constructor(props) {
    super(props);
    this.state = {
      shoutTxt: ''
    };

    this.handleChangeText = this.handleChangeText.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
  }

	handleChangeText(value){
		//console.log('value '+value);
		this.setState({ shoutTxt: value });
	}

	handleSubmit(event){
		//console.log(this.state);
		if(this.state.shoutTxt == ''){
			Toast.show({
        text: "Enter Valid shout message",
        duration: 2000,
        position: "top",
        textStyle: { textAlign: "center" }
      });
		}else{
			//console.log(this.state.shoutTxt);
					//Call API
					fetch('https://jaabnews.com/line_webhook_shout.php?token=xomaod')
					.then((res) => console.log(res.text()));
		}
	}

	render() {
		const param = this.props.navigation.state.params;
		const myProps = {
			comp: '',
			pid: '',
			title: ''
		}

		//Default components
		const compDef = <Text>Blank</Text>
		//Sidebar components
		const compSidebar = <Text>Sidebar</Text>
		//Shout components
		const compShout = <Form>
            <Textarea name="shoutTxt" onChangeText={this.handleChangeText} rowSpan={5} bordered placeholder="Message" />
						<View padder>
							<Button block onPress={this.handleSubmit}>
								<Text>Send</Text>
							</Button>
						</View>
          </Form>

		//Triggers components
		const compTriggers = <Text>Triggers</Text>


		//Check Page type
		if(param){
				switch(param.name.item){
					case "Bot Shout":
					  myProps.comp = compShout;
					  myProps.pid = 'bot_shout';
						myProps.title = 'Shout Message';
						break;
					case "Bot Triggers":
						myProps.comp = compTriggers;
						myProps.pid = 'bot_triggers';
						myProps.title = 'Manage Triggers';
						break;
					default:
						myProps.comp = compDef,
						myProps.pid = 'blank';
						myProps.title = 'Blank';
				}
		}else{
			myProps.comp = compSidebar,
			myProps.pid = 'sidebar_blank';
			myProps.title = 'Sidebar';
		}


		return <BlankPage
							navigation={this.props.navigation}
							{...myProps}
					/>;
	}
}
